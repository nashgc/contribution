async function main() {
  const Contribution = await hre.ethers.getContractFactory("Contribution");
  const contribution = await Contribution.deploy();

  console.log("Contribution deployed to:", contribution.address);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
