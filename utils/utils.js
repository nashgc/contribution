async function getContributionContract(hre, contract) {
  const Contribution = await hre.ethers.getContractFactory("Contribution");
  return await Contribution.attach(contract);
}

module.exports = { getContributionContract };
