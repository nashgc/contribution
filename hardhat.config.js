/**
 * @type import('hardhat/config').HardhatUserConfig
 */
require("@nomiclabs/hardhat-waffle");
require("solidity-coverage");
const dotenv = require("dotenv");
require("./task/task");

dotenv.config({ path: __dirname + "/.env" });
const { ALCHEMY_API_KEY, RINKEBY_API_KEY } = process.env;

if (!ALCHEMY_API_KEY) {
  throw new Error("Please provide your ALCHEMY_API_KEY in a .env file");
}

if (!RINKEBY_API_KEY) {
  throw new Error("Please provide your RINKEBY_API_KEY in a .env file");
}

module.exports = {
  solidity: "0.8.0",
  networks: {
    rinkeby: {
      url: `https://eth-rinkeby.alchemyapi.io/v2/${ALCHEMY_API_KEY}`,
      accounts: [`${RINKEBY_API_KEY}`],
    },
  },
};
