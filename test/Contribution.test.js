const { expect } = require("chai");
const { ethers } = require("hardhat");

describe("Contribution", () => {
  beforeEach(async function () {
    [owner, acc1, acc2, acc3] = await ethers.getSigners();

    Contribution = await ethers.getContractFactory("Contribution");
    contribution = await Contribution.deploy();
  });

  it("Should be deployed", () => {
    expect(contribution.address).to.be.properAddress;
  });

  it("Requires a contribution value > 0", async () => {
    await expect(
      owner.sendTransaction({
        to: contribution.address,
        value: 0,
      })
    ).to.be.revertedWith("Contribution has to be greater than zero");
  });

  it("Should receive currency", async () => {
    await expect(() =>
      owner.sendTransaction({ to: contribution.address, value: 10 })
    ).to.changeEtherBalance(owner, -10);
  });

  it('Should add contributor only once', async () => {
    await owner.sendTransaction({ to: contribution.address, value: 10 });
    await owner.sendTransaction({ to: contribution.address, value: 10 });
    await acc1.sendTransaction({ to: contribution.address, value: 10 });
    await acc1.sendTransaction({ to: contribution.address, value: 10 });

    contributors = await contribution.get_all_contributor();
    expect(contributors[0]).to.be.equal(owner.address);
    expect(contributors[1]).to.be.equal(acc1.address);
  });

  it("Should get all contributors", async () => {
    await owner.sendTransaction({ to: contribution.address, value: 10 });
    await acc1.sendTransaction({ to: contribution.address, value: 10 });
    contributors = await contribution.get_all_contributor();
    expect(contributors[0]).to.be.equal(owner.address);
    expect(contributors[1]).to.be.equal(acc1.address);
  });

  it("Should get total sum of contribution by address", async () => {
    sum_by_owner = await contribution.get_contributor_total_sum(owner.address);
    expect(sum_by_owner).to.be.equal(0);

    await owner.sendTransaction({ to: contribution.address, value: 10 });
    sum_by_owner = await contribution.get_contributor_total_sum(owner.address);
    expect(sum_by_owner).to.be.equal(10);

    await acc1.sendTransaction({ to: contribution.address, value: 11 });
    sum_by_acc1 = await contribution.get_contributor_total_sum(acc1.address);
    expect(sum_by_acc1).to.be.equal(11);
  });

  it("Should fail withradwal money if you are not the owner", async () => {
    await expect(
      contribution.connect(acc1).withdraw(owner.address, 10)
    ).to.be.revertedWith("Only owner can withdraw money");
  });

  it("Should fail withradwal money because contract balance is 0", async () => {
    await expect(
      contribution.connect(owner).withdraw(owner.address, 10)
    ).to.be.revertedWith("The amount of withdraw is exceeded the balance.");
  });

  it("Should withradwal money", async () => {
    await expect(() =>
      owner.sendTransaction({ to: contribution.address, value: 10 })
    ).to.changeEtherBalance(owner, -10);

    await expect(() =>
        contribution.connect(owner).withdraw(owner.address, 10)
    ).to.changeEtherBalance(owner, 10);

  });
});
